@extends('layouts/temp')
@section('content')
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Tambah Staff Dosen</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <form action="/user/{{$data->id}}" method="post">
            @csrf
            @method('put')
            <div class="form-group row">
                <div class="col-sm-6 mb-3 mb-sm-0 ml-2">
                    <p><b>Nama</b>
                        <input type="text" class="form-control" value="{{$data->name}}" id="name" name="name"
                        placeholder="Nama">
                    </p>
                </div>
                <div class="col-sm-6 mb-3 mb-sm-0 ml-2">
                    <p><b>Email</b>
                        <input type="email" class="form-control" value="{{$data->email}}" id="email" name="email"
                        placeholder="Email">
                    </p>
                </div>
                <div class="col-sm-6 mb-3 mb-sm-0 ml-2">
                    <p><b>Role</b>
                        <select name="role" id="role">
                            <option value="admin" {{$data->role=='admin' ? 'selected' : ''}}>Admin</option>
                            <option value="kasir" {{$data->role=='kasir' ? 'selected' : ''}}>Kasir</option>
                        </select>
                    </p>
                </div>
            </div>
            <button class="ml-2" type="submit">Submit</button>
        </form>
    </div>
</div>
@endsection