@extends('layouts/temp')
@section('content')
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Tambah Staff Dosen</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <form action="/barang/{{$data->id}}" method="post">
            @csrf
            @method('put')
            <div class="form-group row">
                <div class="col-sm-6 mb-3 mb-sm-0 ml-2">
                    <p><b>Nama</b>
                    <input type="text" class="form-control" id="nama" name="nama" value="{{$data->nama_barang}}"
                    placeholder="Nama">
                </p>
            </div>
            <div class="col-sm-6 mb-3 mb-sm-0 ml-2">
                <p><b>Harga</b>
                    <input type="number" class="form-control" id="harga" name="harga" value="{{$data->harga_satuan}}"
                    placeholder="Harga">
                </p>
            </div>
            </div>
            <button class="ml-2" type="submit">Submit</button>
        </form>
    </div>
</div>
@endsection