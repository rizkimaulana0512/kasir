@extends('layouts/temp')
@section('content')
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Tambah Barang</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <form action="/barang" method="post">
            @csrf
            @method('post')
            <div class="form-group row">
                <div class="col-sm-6 mb-3 mb-sm-0 ml-2">
                    <p><b>Nama</b>
                    <input type="text" class="form-control" id="nama" name="nama"
                    placeholder="Nama">
                </p>
            </div>
            <div class="col-sm-6 mb-3 mb-sm-0 ml-2">
                <p><b>Harga</b>
                    <input type="number" class="form-control" id="harga" name="harga"
                    placeholder="Harga">
                </p>
            </div>
            </div>
            <button class="ml-2" type="submit">Submit</button>
        </form>
    </div>
</div>
@endsection