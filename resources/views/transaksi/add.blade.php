@extends('layouts/temp')
@section('content')
<div class="container-fluid">
    <!-- Page Heading -->

    <!-- DataTales Example -->
    <div class="card shadow mb-4 row">
        <div clas="ml-4">
            <form action="/trans" method="post">
                @csrf
                @method('post')
                <div class="form-group" id="isian">
                    <div class="col-sm-6 mb-3 mb-sm-0 ml-2">
                        <h5>Total <input type="number" name="total"  id ="total" disabled></h5>
                        <input type="number"  name="totals" id ="totals" hidden>
                        <p> <b>Nama Barang</b>
                            <select class="barang form-control" name="barang[0]" id="barang0"></select>
                        </p>
                        <p>
                            <b>Banyak</b>
                            <input class="form-control" type="number" name="jumlah[0]" id="jumlah0">
                        </p>
                    </div>
                </div>
                <button type="button" class="btn btn-warning ml-3" id="ttl" onClick="ttls()">Total</button>
                <button class="ml-3 btn btn-primary" type="button" id="tmbh">Tambah</button>
                <button class="btn btn-success ml-2" type="submit" >Submit</button>
            </form>
        </div>
        <div class="ml-4">
        </div>
    </div>
</div>
@endsection
@push('script')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script type="text/javascript">
    var i = 0
    var jumlah=[]
    
    $('#barang'+i).select2({
                    placeholder: 'Select an item',
                    ajax: {
                    url: '/select2-autocomplete-ajax',
                    dataType: 'json',
                    delay: 250,
                    processResults: function (data) {
                        return {
                        results:  $.map(data, function (item) {
                                return {
                                    text: item.nama_barang,
                                    id: item.id
                                }
                            })
                        };
                    },
                    cache: true
                    }
                });
    document.getElementById("tmbh").onclick = function() {Tambah()};
    // var x = document.querySelector('input[name="Ada"]:checked');
            function Tambah(){
                ++i;
                document.getElementById("isian").insertAdjacentHTML('beforeend',`
                <div class="col-sm-6 mb-3 mb-sm-0 ml-2">
                        <p> <b>Nama Barang</b>
                            <select class="barang form-control" name="barang[`+i+`]" id="barang`+i+`"></select>
                        </p>
                        <p>
                            <b>Banyak</b>
                            <input class="form-control" type="number" name="jumlah[`+i+`]" id="jumlah`+i+`">
                        </p>
                    </div>
                `);
                $('#barang'+i).select2({
                    placeholder: 'Select an item',
                    ajax: {
                    url: '/select2-autocomplete-ajax',
                    dataType: 'json',
                    delay: 250,
                    processResults: function (data) {
                        return {
                        results:  $.map(data, function (item) {
                                return {
                                    text: item.nama_barang,
                                    id: item.id
                                }
                            })
                        };
                    },
                    cache: true
                    }
                });
                // totals();

            };
            // function totals(){
            //     var input = document.getElementById("jumlah"+i);
            //     input.addEventListener("keyup",function(event){
            //         var total = 0
            //         var harga= 0;
            //         var barang = document.getElementById("barang"+i).value;
            //         var banyak = input.value;
            //         @foreach($data as $key=>$value)
            //         var id ={{$value->id}}
            //         if(id == barang){
            //             harga = {{$value->harga_satuan}}
            //         }
            //         jumlah[barang] = harga * banyak
            //         @endforeach
            //         var ids = []
            //         if(ids.includes(barang)){

            //         }else
            //             ids.push(barang);
            //         for(var j =0;j<=i ;j++){
            //             // console.log(ids[j]+" "+jumlah[ids[j]]+" "+total)
            //             total = total + jumlah[ids[j]]
            //         }
            //         for(var j =0 ;j<=ids.length-1;j++){
            //             console.log(ids.length);
            //         }
            //         // console.log(data)
            //         document.getElementById("total").value= total;
            //     })
            // }
            // totals();
            
            function ttls(){
                var total = 0
                for(var j = 0 ; j<=i ; j++){
                    var jumlah = document.getElementById("jumlah"+j).value;
                    var barang = document.getElementById("barang"+j).value;
                    @foreach($data as $key=>$value)
                        var id ={{$value->id}}
                        if(id == barang){
                            harga = {{$value->harga_satuan}}
                        }
                    @endforeach
                    total += harga * jumlah;
                    document.getElementById("total").value= total;
                    document.getElementById("totals").value= total;
                }
            }

</script>
@endpush