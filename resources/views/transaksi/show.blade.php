@extends('layouts/temp')
@section('content')
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Tables</h1>
<p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below.
    For more information about DataTables, please visit the <a target="_blank"
        href="https://datatables.net">official DataTables documentation</a>.</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <a href="/trans/print/{{$id}}" class="btn btn-primary"><i class="fas fa-plus"></i>&nbsp Print</a>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Waktu Transaksi</th>
                        <th>Id Transaksi</th>
                        <th>Id Barang</th>
                        <th>Jumlah Barang</th>
                        <th>Harga Satuan</th>
                        <th></th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Waktu Transaksi</th>
                        <th>Id Transaksi</th>
                        <th>Id Barang</th>
                        <th>Jumlah Barang</th>
                        <th>Harga Satuan</th>
                        <th></th>
                    </tr>
                </tfoot>
                <tbody>
                    @foreach($data as $key=>$value)
                        <tr>
                            <td>{{$value->created_at}}->format('l d F Y H:i')}}</td>
                            <td>{{$value->transaksi_pembelian_id}}</td>
                            <td>{{$value->master_barang_id}}</td>
                            <td>{{$value->jumlah}}</td>
                            <td>{{$value->harga_satuan}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

</div>
@endsection