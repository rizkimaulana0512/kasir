@extends('layouts/temp')
@section('content')
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Tables</h1>
<p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below.
    For more information about DataTables, please visit the <a target="_blank"
        href="https://datatables.net">official DataTables documentation</a>.</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <a href="/trans/add" class="btn btn-primary"><i class="fas fa-plus"></i>&nbsp Tambah Data</a>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Waktu Transaksi</th>
                        <th>Harga</th>
                        <th></th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Nama</th>
                        <th>Waktu Transaksi</th>
                        <th>Harga</th>
                        <th></th>
                    </tr>
                </tfoot>
                <tbody>
                    @foreach($data as $key=>$value)
                        <tr>
                            <td>{{$value->id}}</td>
                            <td>{{$value->created_at->format('l d F Y H:i')}}</td>
                            <td>{{$value->total_harga}}</td>
                            <td>
                                <form action="/barang/{{$value->id}}" method="post">
                                    @csrf
                                    @method('delete')
                                    <a href="/trans/{{$value->id}}" class="btn btn-success"><i class="fas fa-eye fa-sm text-white-10"></i>&nbsp Detail</a>
                                    <!-- <a href="/barang/edit/{{$value->id}}" class="btn btn-success"><i class="fas fa-pencil-alt fa-sm text-white-10"></i>&nbsp Edit</a> -->
                                    <button type="submit" class="btn btn-danger"><i class="fas fa-trash-alt fa-sm text-white-10"></i>&nbsp Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

</div>
@endsection