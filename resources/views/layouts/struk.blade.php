<div>
    <div style="text-align:center">
        <h1>Struk Transaksi</h1>
    </div>
    <hr>
    <div>
        <table style="width:100%">
            <thead>
                <th>#</th>
                <th>Nama Barang</th>
                <th>Harga</th>
                <th>Jumlah</th>
                <th>Total</th>
            </thead>
            <tbody style="text-align:center">
                @foreach($data as $key=>$value)
                <tr >
                    <td>{{$key +1}}</td>
                    <td>{{$value->nama_barang}}</td>
                    <td>{{$value->harga_satuan}}</td>
                    <td>{{$value->jumlah}}</td>
                    <td>{{$value->jumlah * $value->harga_satuan}}</td>
                </tr>
                @endforeach
                <tr>
                    <td><hr></td>
                    <td><hr></td>
                    <td><hr></td>
                    <td><hr></td>
                    <td><hr></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>Total</td>
                    <td>{{$banyak}}</td>
                    <td>{{$total}}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
    