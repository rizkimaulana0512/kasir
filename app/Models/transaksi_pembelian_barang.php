<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class transaksi_pembelian_barang extends Model
{
    use HasFactory;
    protected $guarded=[];

    public function struk(){
        return $this->belongsTo(transaksi_pembelian::class);
    }

    public function barang(){
        return $this->belongsTo('App\Models\master_barang');
    }
}
