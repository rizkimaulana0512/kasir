<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class transaksi_pembelian extends Model
{
    use HasFactory;
    protected $guarded=[];
    
    public function struk(){
        return $this->hasMany(transaksi_pembelian_barang::class);
    }
}
