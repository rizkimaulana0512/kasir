<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class master_barang extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function struk (){
        return $this->belongsto(transaksi_pembelian_barang::class);
    }
}
