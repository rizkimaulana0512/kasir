<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\transaksi_pembelian as Trans;
use App\Models\transaksi_pembelian_barang as Struk;
use App\Models\master_barang as Barang;
use Carbon\Carbon;
class Transaksi extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        $this->middleware('auth');
    }
    public function index()
    {

        if(\Auth::user()->role == 'kasir'){
            
            $data = Trans::whereDate('created_at',Carbon::now())->get();
            
            
        }else if(\Auth::user()->role == 'admin'){
            $data = Trans::get();
        }
        return view('transaksi.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = Barang::get();
        return view('transaksi.add',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        echo $request->totals;
        $trans = Trans::create(['total_harga'=>$request->totals]);
        foreach($request->barang as $key=>$value){
            $barang = Barang::where('id',$request->barang[$key])->first();
            $data = array(
                'transaksi_pembelian_id' => $trans->id,
                'master_barang_id' => $request->barang[$key],
                'jumlah' => $request->jumlah[$key],
                'harga_satuan' => $barang->harga_satuan,
                
            );
                Struk::create($data);
        }
        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Struk::where('transaksi_pembelian_id',$id)->get();
        return view('transaksi.show',compact('data','id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function print($id){
        $total=0;
        $banyak = 0; 
        $data = Struk::where('transaksi_pembelian_id',$id)
                ->join('master_barangs','master_barangs.id','master_barang_id')
                ->get();
        foreach($data as $key=>$value){
            $banyak += $value->jumlah;
            $total += $value->harga_satuan * $value->jumlah;
        }
        // echo $total;
        $pdf = \App::make('dompdf.wrapper');
        return $pdf->loadView('layouts.struk', compact('data','total','banyak'))->stream();

    }
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
