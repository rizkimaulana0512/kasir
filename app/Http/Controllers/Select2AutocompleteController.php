<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\master_barang as Barang;

class Select2AutocompleteController extends Controller
{
    public function layout()
    {
    	return view('select2');
    }

    public function dataAjax(Request $request)
    {
    	$data = [];


        if($request->has('q')){
            $search = $request->q;
            $data = Barang::select("id","nama_barang")
            		->where('nama_barang','LIKE',"%$search%")
            		->get();
        }
        else{
            $data = Barang::select("id","nama_barang")
                    ->get();
        }

        // echo $data;
        return response()->json($data);
    }

}
