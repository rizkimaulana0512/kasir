<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\master_barang as Barangs;
use middleware;

class Barang extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        $this->middleware(['auth']);
    }
    public function index()
    {
        $data = Barangs::get();
        return view('barang.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('barang.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData  = $request->validate(
            [
                'nama' => 'required',
                'harga' => 'required'
            ],
            [
                'nama.require' => 'Harap Masukan Nama Barang',
                'harga.require' => 'Harap Masukan Harga Barang'
            ]
        );
        $data = array (
            'nama_barang' => $request->nama,
            'harga_satuan'=> $request->harga
        );

        Barangs::create($data);
        return redirect('/barang');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Barangs::where('id',$id)->first();
        return view('barang.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validateData  = $request->validate(
            [
                'nama' => 'required',
                'harga' => 'required'
            ],
            [
                'nama.require' => 'Harap Masukan Nama Barang',
                'harga.require' => 'Harap Masukan Harga Barang'
            ]
        );
        $data = array (
            'nama_barang' => $request->nama,
            'harga_satuan'=> $request->harga
        );

        Barangs::where('id',$id)->update($data);
        return redirect('/barang');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Barangs::where('id',$id)->delete();
        return redirect('/barang');
    }
}
