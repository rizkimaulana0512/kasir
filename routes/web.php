<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Barang;
use App\Http\Controllers\User;
use App\Http\Controllers\Transaksi;
use App\Http\Controllers\Select2AutocompleteController as Auto;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Route Barang
Route::get('/barang',[Barang::class,'index']);
Route::post('/barang',[Barang::class,'store']);
Route::get('/barang/add',[Barang::class,'create']);
Route::put('/barang/{id}',[Barang::class,'update']);
Route::delete('/barang/{id}',[Barang::class,'destroy']);
Route::get('/barang/edit/{id}',[Barang::class,'edit']);

//Rote User
Route::get('/user',[User::class,'index']);
Route::post('/user',[User::class,'store']);
Route::get('/user/add',[User::class,'create']);
Route::put('/user/{id}',[User::class,'update']);
Route::delete('/user/{id}',[User::class,'destroy']);
Route::get('/user/edit/{id}',[User::class,'edit']);


Route::get('/trans',[Transaksi::class,'index']);
Route::post('/trans',[Transaksi::class,'store']);
Route::get('/trans/add',[Transaksi::class,'create']);
Route::put('/trans/{id}',[Transaksi::class,'update']);
Route::get('/trans/{id}',[Transaksi::class,'show']);
Route::get('/trans/print/{id}',[Transaksi::class,'print']);
Route::delete('/trans/{id}',[Transaksi::class,'destroy']);
Route::get('/trans/edit/{id}',[Transaksi::class,'edit']);



Route::get('/',[Transaksi::class,'index']);
Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

Route::get('/select2-autocomplete', [Auto::class,'layout']);
Route::get('/select2-autocomplete-ajax', [Auto::class,'dataAjax']);

require __DIR__.'/auth.php';
