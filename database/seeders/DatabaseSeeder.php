<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\master_barang as Barang;
use App\Models\User;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $data = [
            [1,'Sabun batang', 3000],
            [2,'Mi Instan', 2000],
            [3,'Pensil', 1000],
            [4,'Kopi sachet', 1500],
            [5,'Air minum galon', 20000],
        ];
        foreach($data as $key=>$value){
            Barang::create(['id'=>$value[0],'nama_barang'=>$value[1],'harga_satuan'=>$value[2]]);
        }
        User::create(['name'=>'admin','email'=>'admin@admin.com','password'=>Hash::make('admin'),'role'=>'admin']);
    }
}
